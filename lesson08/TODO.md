# Lesson 08 Java Collections Framework Part III

## Задание 1

1. Реализовать класс `CustomTreeMapImpl<K,V>`, который представляет отображение на основе бинарного дерева поиска.
2. Класс `CustomTreeMapImpl` должен реализовывать интерфейс `CustomTreeMap<K,V>` и может хранить объекты любого типа

### Конструкторы

`CustomTreeMapImpl(Comparator<K> comparator)` - ссылка на `CustomTreeMap<K,V>`: #ref

### Критерии приемки

1. Предоставить на проверку Pull Request из ветки `feature/lesson08` в ветку `develop`.
2. Каждый публичный метод класса `CustomTreeMapImpl` должен быть покрыт unit-тестом.
3. !!! Вносить правки в интерфейс `CustomTreeMap<K,V>` нельзя.

## Задание 2

1. Реализовать класс `CustomHashMapImpl<K, V>`, который представляет отображение на основе хеш-таблицы.
2. Класс `CustomHashMapImpl` реализует интерфейс `CustomHashMap<K, V>` и может хранить объекты любого типа

### Конструкторы

`CustomHashMapImpl()` - ссылка на `CustomHashMap<K, V>`: #ref

### Критерии приемки

1. Предоставить на проверку Pull Request из ветки `feature/lesson08` в ветку `develop`.
2. Каждый публичный метод класса `CustomHashMapImpl` должен быть покрыт unit-тестом.
3. !!! Вносить правки в интерфейс `CustomHashMap<K, V>` нельзя.

## Задание 3

1. Реализовать класс `CustomLinkedHashMapImpl<K, V>`, который представляет отображение на основе хеш-таблицы.
2. Класс `CustomLinkedHashMapImpl` реализует интерфейс `CustomLinkedHashMap<K, V>`.
3. Класс CustomLinkedHashMapImpl может хранить объекты любого типа
4. Методы `toString` и `keys` должны возвращать элементы в том порядке, в котором элементы были добавлены в отображение.

##Конструкторы

`CustomLinkedHashMapImpl()` - ссылка на `CustomLinkedHashMap<K, V>`: #ref

### Критерии приемки

1. Предоставить на проверку Pull Request из ветки `feature/feature08` в ветку `develop`
2. Каждый публичный метод класса `CustomLinkedHashMapImpl` должен быть покрыт unit-тестом.
3. !!! Вносить правки в интерфейс `CustomLinkedHashMap<K, V>` нельзя
