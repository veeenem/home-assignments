package ru.sberbank.jd.lesson01;

import java.util.Collection;

public interface Greeting {

  /**
   * Get first name.
   */
  String getFirstName();

  /**
   * Get second name
   */
  String getSecondName();

  /**
   * Get last name.
   */
  String getLastName();

  /**
   * Get birth year.
   */
  String getBirthYear();

  /**
   * Get hobbies.
   */
  Collection<String> getHobbies();

  /**
   * Get bitbucket url to your repo.
   */
  String getBitbucketUrl();

  /**
   * Get phone number.
   */
  String getPhone();

  /**
   * Your expectations about course.
   */
  Collection<String> getCourseExpectations();

  /**
   * Print your university and faculty here.
   */
  Collection<String> getEducationInfo();

}
